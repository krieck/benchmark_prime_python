Prime Benchmark
===============

This repository contains a simplistic performance benchmark.
It finds the millionth prime number and then reports the
time required in milliseconds.

I've implemented this in Python and Java

