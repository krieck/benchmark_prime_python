import time;

primes = [ 2 ]

def init(max) :
    n = 3
    while len(primes) < max :
        if isPrime(n) :
            primes.append(n)
        n = n + 2

def isPrime(n) :
    for p in primes :
        if n == p : 
            return True
        if n % p == 0 : 
            return False
        if p * p > n :
            return True
    return True

def test(m) :
    count = m
    n = 2
    millis = time.time() * 1000.0
    while count > 0 :
        if isPrime(n) :
            count = count - 1
        n = n + 1
    millis = time.time() * 1000.0 - millis
    print "Prime number: ", n, " took ", millis

init(600)

test(1000000)
